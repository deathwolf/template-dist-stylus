module.exports = function (options) {

	gulp.task('lint', function () {
		return gulp.src(options.files.js)
			.pipe($.plumber({errorHandler: options.errorHandler("eslint")}))
			.pipe($.changed(options.paths.dist, {extension: '.js'}))
			.pipe($.eslint())
			.pipe($.eslint.format())
	});

};
