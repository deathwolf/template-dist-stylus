module.exports = function (options) {

	gulp.task('scripts', function () {
		return gulp.src(options.files.js)
			.pipe($.plumber(options))
			.pipe($.changed(options.paths.dist, {extension: '.js'}))
			.pipe(gulp.dest(options.paths.dist + '/scripts'))
			.pipe(browserSync.stream());
	});

};
