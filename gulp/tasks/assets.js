module.exports = function (options) {
	gulp.task('assets', function () {
		gulp.src([
			options.files.fonts
		])
			.pipe(gulp.dest(options.paths.dist + '/fonts'))
			.pipe(browserSync.stream());

		return gulp.src(options.files.images)
			.pipe($.plumber({errorHandler: options.errorHandler("assets")}))
			.pipe(gulp.dest(options.paths.dist + '/images'))
			.pipe(browserSync.stream());
	});
};
